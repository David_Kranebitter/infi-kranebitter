CREATE TABLE IF NOT EXISTS Temperaturmessung (
 TemperaturID INTEGER PRIMARY KEY AUTO_INCREMENT,
 Temperatur INT NOT NULL,
 Datum VARCHAR(50) NOT NULL);