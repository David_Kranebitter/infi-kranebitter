import java.awt.Dimension;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYSplineRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

public class DBManager {

	private static ArrayList<Temperature> temList = new ArrayList<Temperature>();
	private static Connection c;
	private String pw = "Iquoxum321";
	private String databaseName = "Temperaturmessung";
	private Scanner sc = new Scanner(System.in);

	DBManager() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			c = DriverManager.getConnection("jdbc:mysql://localhost/" + databaseName, "root", pw);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void createTable() throws SQLException, IOException {
		String sql = "";
		ArrayList<String> list = null;
		Statement stmt = null;
		Reader r = new Reader();
		try {
			stmt = c.createStatement();
			list = r.readIn();
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		for (int i = 0; i < list.size(); i++) {
			if (!(list.get(i).contains(");"))) {
				sql += list.get(i);
			} else {
				sql += list.get(i);
				try {
					stmt.executeUpdate(sql);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					if (stmt != null) {
						stmt.close();
					}
				}
				sql = "";

			}

		}

	}

	public ArrayList<Temperature> select(int limit) throws SQLException {
		String sql = "SELECT * FROM temperaturmessung LIMIT ?";
		PreparedStatement stmt = null;
		ArrayList<Temperature> list = new ArrayList<Temperature>();

		try {
			System.out.println("Limit: ");

			stmt = c.prepareStatement(sql);
			stmt.setInt(1, limit);
			sql = "SELECT * FROM temperaturmessung LIMIT " + limit;
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				list.add(new Temperature(rs.getInt(2), rs.getString(3)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				stmt.close();
			}
		}
		return list;
	}
	
	public void delete() throws SQLException {
		String sql = "Delete from temperaturmessung";
		temList.clear();
		PreparedStatement stmt = null;
		
		try {
			stmt = c.prepareStatement(sql);
			stmt.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			if(stmt != null) {
				stmt.close();
			}
		}
	}

	public void insertInto(Temperature tem) throws SQLException {
		String sql = "INSERT INTO Temperaturmessung (Temperatur, Datum) VALUES (?,?)";
		PreparedStatement stmt = null;
		try {
			stmt = c.prepareStatement(sql);
			stmt.setInt(1, tem.getTemperature());
			stmt.setString(2, tem.getDateStr());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
					stmt.close();				
				}
			}
		}

	

	public void menue() throws SQLException {

		while (true) {
			System.out.println("Welche Operation wollen sie ausf�hren?");
			System.out.println("Daten einf�gen ... 1");
			System.out.println("Daten auslesen ... 2");
			System.out.println("Alle Daten l�schen ... 3");
			System.out.println("Zuf�llige Daten einf�gen ... 4");
			System.out.println("Diagramm ausgeben ... 5");
			String ans = sc.next();
			switch (ans) {
			case "1":
				insertTem();
				break;
			case "2":
				showTem();
				break;
			case "3":
				delete();
				break;
			case "4":
				insertRndTem();
				break;
			case "5":
				plotTem();
				break;
			default:
				System.out.println("Falsche Eingabe");
				break;
			}

		}
	}

	public void plotTem() throws SQLException {
		XYSeries series1 = new XYSeries("Temperaturverlauf");
		
		temList = new ArrayList<Temperature>();
		for(Temperature element : select(100)) {
			temList.add(element);
		}

		for (int i = 0; i < temList.size(); i++) {
			System.out.println(temList.get(i).getTemperature());
			series1.add(i, temList.get(i).getTemperature());
		}
		
		XYSeriesCollection dataset = new XYSeriesCollection();
		dataset.addSeries(series1);

		XYSplineRenderer dot = new XYSplineRenderer();
		
		NumberFormat nf = NumberFormat.getInstance();
		 for (int i = 0; i < 500; i += 5) {
		    nf.format(i);
		 }

		NumberAxis xax = new NumberAxis("Zeit");
		xax.setNumberFormatOverride(nf);
		NumberAxis yax = new NumberAxis("Temperatur");
		
		yax.setRange(-35.0, 35.0);
		xax.setRange(0.0, 50.0);

		XYPlot plot = new XYPlot(dataset, xax, yax, dot);

		JFreeChart chart2 = new JFreeChart(plot);

		ApplicationFrame punkteframe = new ApplicationFrame("Temperaturmesskurve");
		
		ChartPanel chartPanel2 = new ChartPanel(chart2);
		chartPanel2.setPreferredSize(new Dimension(1900, 950));
		punkteframe.setContentPane(chartPanel2);
		punkteframe.pack();
		punkteframe.setVisible(true);
	}

	public void insertRndTem() throws SQLException {
		int range = 0;
		System.out.println("Wieviele zuf�llige Temperaturen sollen eingef�gt werden?: ");
		range = Integer.parseInt(sc.next());
		int startTem = (int) (Math.random() * 60) - 30;
		Temperature t = new Temperature(startTem);
		
		temList.add(t);
		insertInto(t);
		for (int i = 1; i <= range; i++) {
			temList.add(new Temperature(temList.get(i - 1).getTemperature()));
			insertInto(temList.get(i));
		}
	}

	public void insertTem() throws SQLException {
		String temDate = "";
		int tem = 0;
		String temYear = "";
		String temMonth = "";
		String temDay = "";
		boolean errInput = false;
		boolean errTem = false;
		boolean errDate = false;
		do {
			System.out.println("Bitte Temperatur eingeben: ");
			try {
				errTem = false;
				tem = Integer.parseInt(sc.next());
			} catch (Exception e) {
				System.out.println("Falsche Eingabe");
				errTem = true;
			}
		} while (errTem);
		do {
			System.out.println("Datum angeben?: [J/N]");
			switch (sc.next()) {
			case "J":
				do {
					System.out.println("Bitte Datum eingeben");
					System.out.println("Jahr: ");
					temYear = sc.next();
					System.out.println("Monat");
					temMonth = sc.next();
					System.out.println("Day");
					temDay = sc.next();
					try {
						errDate = false;
						Integer.parseInt(temYear);
						Integer.parseInt(temMonth);
						Integer.parseInt(temDay);
					} catch (Exception e) {
						System.out.println("Falsche Eingabe");
						errDate = true;
					}
				} while (errDate);

				temDate = temYear + "-" + temMonth + "-" + temDay;
				System.out.println(temDate);
				errInput = false;
				break;
			case "N":
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");
				temDate = sdf.format(date);
				System.out.println(temDate);
				errInput = false;
				break;
			default:
				System.out.println("Falsche Eingabe");
				errInput = true;
				break;
			}
		} while (errInput);

		Temperature t = new Temperature(tem, temDate);
		insertInto(t);
	}

	public void showTem() throws SQLException {
		int limit = 0;
		boolean err = false;
		do {
			err = false;
			System.out.println("Limit: ");
			String sLimit = sc.next();
			try {
				limit = Integer.parseInt(sLimit);
			} catch (Exception e) {
				System.out.println("Falsche Eingabe");
				err = true;
			}
		} while (err);
		for (Temperature element : select(limit)) {
			System.out.println("-------------------------------------------------------------------------");
			System.out.println("Temperatur: " + element.getTemperature());
			System.out.println("Datum: " + element.getDateStr());
			System.out.println();
		}

	}

}
