import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Temperature {

	private static int minuteCounter = 5;
	private int temID;
	private int temperature;
	private Calendar cal = Calendar.getInstance();
	private Date dateTem = new Date();
	private String dateStr;
	private Random rnd = new Random();
	private SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss");
	
	
	public Temperature(int temperature, String dateStr) {
		this.temperature = temperature;
		this.dateStr = dateStr;
	}
	
	public Temperature(int prevTem) {
		genTem(prevTem);
		cal.set(Calendar.MINUTE, minuteCounter);
		//Date date =  new Date();
		dateStr = sdf.format(cal.getTime());
		
		if(minuteCounter == 60) {
			minuteCounter = 0;
		}else {
			minuteCounter += 5;
		}
	}
	
	public void genTem(int prevTem) {
		temperature = prevTem + rnd.nextInt(3) - 1;
	}

	public int getTemID() {
		return temID;
	}

	public void setTemID(int temID) {
		this.temID = temID;
	}

	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public Date getDateTem() {
		return dateTem;
	}

	public void setDateTem(Date dateTem) {
		this.dateTem = dateTem;
	}

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	
	
	
	
}
