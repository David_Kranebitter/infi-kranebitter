import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.Timer;

public class DBManager implements ActionListener {

	private static ArrayList<Temperature> temList = new ArrayList<Temperature>();
	private int iCounter = 1;
	private static Connection c;
	private String pw = "Iquoxum321";
	private String databaseName = "Temperaturmessung";
	Timer t = new Timer(1000, this);

	DBManager() throws SQLException {
		try {
			t.start();
			Class.forName("com.mysql.jdbc.Driver");
			if (!(System.getProperty("user.dir").contains("David"))) {
				Scanner sc = new Scanner(System.in);
				System.out.println("Bitte Passwort eingeben: ");
				pw = sc.next();
				System.out.println("Bitte den Namen der Datenbank eingeben: ");
				databaseName = sc.next();
				sc.close();
			}
			c = DriverManager.getConnection("jdbc:mysql://localhost/" + databaseName, "root", pw);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		int startTem = (int) (Math.random() * 60 - 30);
		temList.add(new Temperature(startTem));
		insert(temList.get(0));
	}


	public static void createTable() {
		String sql = "";
		ArrayList<String> list = null;
		Statement stmt = null;
		Reader r = new Reader();
		try {
			stmt = c.createStatement();
			list = r.readIn();
			for (String st : list) {
				System.out.println(st);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < list.size(); i++) {

			if (!(list.get(i).contains(");"))) {
				sql += list.get(i);
			} else {
				sql += list.get(i);
				try {
					stmt.executeUpdate(sql);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				sql = "";

			}

		}

	}

	public static void insert(Temperature tem) {
		String sql = "INSERT INTO Temperaturmessung (Temperatur, Datum) VALUES (?,?)";
		PreparedStatement stmt = null;

		try {
			stmt = c.prepareStatement(sql);
			stmt.setInt(1, tem.getTemperature());
			stmt.setString(2, tem.getDateStr());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public static void main(String[] args) throws SQLException {
	
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		temList.add(new Temperature(temList.get(iCounter - 1).getTemperature()));
		insert(temList.get(iCounter));
		iCounter++;
	}

}
